# -*- coding: utf-8 -*-
"""
Urban Tree Vitality Monitoring

Treemendous Professionals

"""

# Load required packages and the functions file
import os
import geopandas as gpd
import functions as funcs

# Create data and output folders
if not os.path.exists('data'):
    os.makedirs('data')
if not os.path.exists('data/raster'):
    os.makedirs('data/raster') 
if not os.path.exists('data/study_extent'):
    os.makedirs('data/study_extent')
if not os.path.exists('data/tree_points'):
    os.makedirs('data/tree_points')

if not os.path.exists('intermediate'):
    os.makedirs('intermediate')

if not os.path.exists('output'):
    os.makedirs('output')

###### Get shapefile and bounds of Study Area for further operations ######
study_extent = gpd.read_file() # Path to study area
bounds = tuple(study_extent.total_bounds)


###### Process Rasters ######
funcs.reproject()
funcs.clip_rasters(study_extent)
funcs.dark_pixel_correction()


###### Canopy Height Model ######

### AHN3 ###
funcs.save_wcs_to_tiff('https://service.pdok.nl/rws/ahn3/wcs/v1_0?request=getcapabilities&service=wcs', '1.0.0', 'ahn3_05m_dsm', bounds)
funcs.save_wcs_to_tiff('https://service.pdok.nl/rws/ahn3/wcs/v1_0?request=getcapabilities&service=wcs', '1.0.0', 'ahn3_05m_dtm', bounds)
funcs.calculate_chm()
funcs.remove_buildings()

### AHN4 ###
funcs.save_wcs_to_tiff('https://service.pdok.nl/rws/ahn/wcs/v1_0?SERVICE=WCS&request=GetCapabilities', '1.0.0', 'dsm_05m', bounds)
funcs.save_wcs_to_tiff('https://service.pdok.nl/rws/ahn/wcs/v1_0?SERVICE=WCS&request=GetCapabilities', '1.0.0', 'dtm_05m', bounds)
funcs.calculate_chm()
funcs.remove_buildings()


###### Calculate Vegetation Indices ######
funcs.calculate_vi()
funcs.extract_trees_from_VI()
funcs.calculate_average_vi()


###### Trend Analysis ######
funcs.prepare_data()
funcs.linear_regression_slopes(bounds)

