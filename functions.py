#### PROCESSING OPTICAL RASTER DATA ####

def reproject(in_dir, out_dir, crs):
    """
    The reproject function reprojects a tif file into the given projection

    INPUT:
        in_dir (str): A path to the directory in which the tif files are located.
        out_dir (str): A path to the directory in which the reprojected tif files will be saved in.
        crs (str): A transformation for a projection, such as EPSG:28992.
        
    OUTPUT:
        This function does not return. This function saves reprojected tif files to the specified output folder. It will print a message 
        once it is done.
        
    POTENTIAL RISKS & ASSUMPTIONS:
        - This function assumes that the input and output directories will be presented without a trailing slash.
        - This function does not check whether the given input is valid, and thus can crash the code if invalid input is given.
        - This function only looks for .tif files and does not check file names. All TIFF files in the specified input folder are reprojected.
        
    """
    
    # Import necessary packages for function
    import os
    import rasterio
    from rasterio.warp import calculate_default_transform, reproject
    
    # Create the output directory if it doesn't exist
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    # List all TIFF files within the given input directory
    tif_files = [f for f in os.listdir(in_dir) if f.endswith('.tif')]
    
    # Loop through all the TIFF files
    for file in tif_files:
        # Open the TIFF file
        src_path = os.path.join(in_dir, file)
        with rasterio.open(src_path) as src:
            # Reproject the TIFF file to the input CRS and write it to the given output directory
            transform, width, height = calculate_default_transform(src.crs, crs, src.width, src.height, *src.bounds)
            kwargs = src.meta.copy()
            kwargs.update({
                'crs': crs,
                'transform': transform,
                'width': width,
                'height': height
            })
            dst_path = os.path.join(out_dir, file)
            with rasterio.open(dst_path, 'w', **kwargs) as dst:
                for i in range(1, src.count + 1):
                    reproject(
                        source=rasterio.band(src, i),
                        destination=rasterio.band(dst, i),
                        src_transform=src.transform,
                        src_crs=src.crs,
                        dst_transform=transform,
                        dst_crs=crs
                    )
    
    # Print a message that the reprojection process is finished
    print('Reprojecting done')

    
def clip_rasters(in_dir, out_dir, shapefile):
    """
    The clip_rasters function clips raster files to the extent of a specified shapefile.

   INPUT:
       in_dir (str): A string representing the path to the directory where the input raster files are located.
       out_dir (str): A string representing the path to the directory where the clipped raster files will be saved.
       shapefile (gpd df): A geopandas dataframe representing the shapefile used to define the clipping extent.

   OUTPUT:
       This function does not return. The function exports the clipped raster files to the specified output directory.

   POTENTIAL RISKS & ASSUMPTIONS:
       - The function assumes that the input and output directories are valid paths and do not contain a '/' at the end.
       - The function assumes that the input raster files have the .tif extension.
       - The function assumes that the shapefile has a valid geometry and coordinate reference system (CRS).
       - The function overwrites existing files with the same name in the output directory.
       - The function does not perform any checks to ensure that the input and output directories exist or are accessible.
    
    """
    
    # Import necessary packages for function
    import os
    import rasterio
    from rasterio.mask import mask
    import geopandas as gpd
    from shapely.geometry import box
    
    # Create the output directory if it doesn't exist
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
        
    # List all TIFF files within the given input directory
    tif_files = [f for f in os.listdir(in_dir) if f.endswith('.tif')]

    # Reads each file and clips it to the extent of the shapefile
    for file in tif_files:
        # Opens the raster
        src_path = os.path.join(in_dir, file)
        with rasterio.open(src_path) as src:
            # Copy the metadata
            raster_meta = src.meta.copy()
            # Create the bounds of the shapefile in a bbox
            bounds = shapefile.geometry.bounds
            bbox = box(bounds.minx.min(), bounds.miny.min(), bounds.maxx.max(), bounds.maxy.max())
            # Project the bbox in the same crs as the raster
            bbox = gpd.GeoDataFrame(geometry=[bbox], crs=shapefile.crs)
            bbox = bbox.to_crs(src.crs)
            # Clips the raster to the extent of the shapefile
            out_image, out_transform = mask(src, shapes=bbox.geometry, crop=True)
        
        # Updates the raster metadata
        raster_meta.update({
            'height': out_image.shape[1],
            'width': out_image.shape[2],
            'transform': out_transform
        })
        # Export the clipped raster to the output directory
        dst_path = os.path.join(out_dir, file)
        with rasterio.open(dst_path, 'w', **raster_meta) as dst:
            dst.write(out_image)
            
            
def dark_pixel_correction(in_dir, out_dir):
    """
    Performs dark pixel correction on raster files. It finds the lowest reflectance values for each band
    in the input raster, subtracts those values from the corresponding pixels in each band, and saves the corrected
    raster files to the specified output directory.

    INPUT:
        in_dir (str): A path to the directory where the input raster files are located.
        out_dir (str): A path to the directory where the corrected raster files will be saved.

    OUTPUT:
        This function does not return. The corrected rasters are saved in the specified output directory.

    POTENTIAL RISKS & ASSUMPTIONS:
        - The function assumes that the input and output directories are valid paths and do not contain a trailing slash.
        - The function assumes that the input raster files have the .tif extension.
        - The function overwrites existing files with the same name in the output directory.
        
    """

    # Import necessary packages for function
    import numpy as np
    import os
    import rasterio

    # Get a list of all raster files in the input folder
    raster_files = os.listdir(in_dir)
    raster_files = [file for file in raster_files if file.endswith('.tif')]

    for raster_file in raster_files:
        # Set the path to the current input raster
        input_raster_path = os.path.join(in_dir, raster_file)

        # Read the input raster
        input_raster = rasterio.open(input_raster_path)

        # Read all the bands of the input raster
        input_raster_bands = input_raster.read()

        # Find the lowest reflectance values for each band
        lowest_reflectance_values = []
        for band in input_raster_bands:
            band_data = band.astype(np.float32)  # Convert the band data to float32
            valid_pixels = band_data[band_data > 1]  # Exclude pixels with value 1 or lower
            lowest_reflectance_values.append(np.sort(valid_pixels.flat)[0])

        lowest_reflectance_values = np.array(lowest_reflectance_values)  # Convert to NumPy array

        print("Lowest Reflectance Values:", lowest_reflectance_values)

        # Perform darkest pixel correction
        darkest_subtracted_raster = np.where(
            input_raster_bands > lowest_reflectance_values[:, np.newaxis, np.newaxis],
            input_raster_bands - lowest_reflectance_values[:, np.newaxis, np.newaxis],
            input_raster_bands  # Set pixels with values lower than the lowest reflectance value to original value
        )

        # Copy the input raster's profile for the output raster
        output_profile = input_raster.profile

        # Create the output directory if it doesn't exist
        os.makedirs(out_dir, exist_ok=True)

        # Create the output raster file path
        output_raster_path = os.path.join(out_dir, "corrected_" + os.path.basename(input_raster_path))

        # Check if the output file already exists
        if os.path.exists(output_raster_path):
            print("Output raster file already exists. Skipping saving step.")
        else:
            # Write the darkest subtracted raster to the output raster file
            with rasterio.open(output_raster_path, 'w', **output_profile) as output_raster:
                output_raster.write(darkest_subtracted_raster)
            print("Output raster file saved successfully.")

        # Read the corrected raster
        corrected_raster = rasterio.open(output_raster_path)
        corrected_raster_bands = corrected_raster.read()

        # Calculate statistics for the original raster
        original_stats = []
        for band_idx, band in enumerate(input_raster_bands, start=1):
            stats = {
                'minimum': band.min(),
                'maximum': band.max(),
                'mean': band.mean()
            }
            original_stats.append(stats)
            print(f"Original Raster Band {band_idx} Statistics:", stats)

        # Calculate statistics for the corrected raster
        corrected_stats = []
        for band_idx, band in enumerate(corrected_raster_bands, start=1):
            stats = {
                'minimum': band.min(),
                'maximum': band.max(),
                'mean': band.mean()
            }
            corrected_stats.append(stats)
            print(f"Corrected Raster Band {band_idx} Statistics:", stats)
    

#### CANOPY HEIGHT MODEL ####
    
def save_wcs_to_tiff(url, version, identifier, bounds, out_dir):
    """
   Saves a WCS (Web Coverage Service) into a TIFF file.

    INPUT:
        url (str): The URL of the WCS service.
        version (str): The version of the WCS service
        identifier  (str): The identifier of the WCS to be retrieved
        bounds (tuple): A tuple representing the bounding box of the study area in the format (minx, miny, maxx, maxy).
        output_dir (str): A path to the directory in which the TIFF file will be saved.
        
    OUTPUT:
        This function does not return. This function saves TIFF file recieved to the specified output folder. 

    POTENTIAL RISKS & ASSUMPTIONS:
        - This function does not check whether the given input is valid and thus can return an error if invalid input is given.
        - This function only works with WCS files, and assumes that the provided WCS files are in the correct format.

    """
    
    # Import necessary packages for function
    import os
    from owslib.wcs import WebCoverageService
    
    # Create the output directory if it doesn't exist
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # Access the WCS by providing the URL of the WCS service
    wcs = WebCoverageService(url, version='1.0.0')

    # Request the data from the WCS
    if 'ahn3' in identifier:
        response = wcs.getCoverage(identifier, bbox=bounds, format='GEOTIFF_FLOAT32',
                               crs='urn:ogc:def:crs:EPSG::28992', resx=0.5, resy=0.5)
    else:
        response = wcs.getCoverage(identifier, bbox=bounds, format='GEOTIFF',
                               crs='urn:ogc:def:crs:EPSG::28992', resx=0.5, resy=0.5)

    # Save the data to a TIFF file
    with open(out_dir+'/'+identifier+'.tif', 'wb') as file:
        file.write(response.read())
        

def calculate_chm(dsm_file, dtm_file, out_dir, filename):
    """
    Calculates the Canopy Height Model (CHM) by subtracting the Digital Terrain Model (DTM) from the Digital Surface
    Model (DSM) and writes it to the output directory.

    INPUT:
        dsm_file (str): Path to the DSM raster file.
        dtm_file (str): Path to the DTM raster file.
        output_dir (str): Path to save the output CHM raster file.
        filename (str): The name of the output CHM raster file
    
    OUTPUT:
        This function does not return. The CHM raster will be saved in the specified output directory.
   
    POTENTIAL RISKS & ASSUMPTIONS:
        - Assumes missing values have been filled (see function fill_nodata_values)
    

    """
    
    # Import necessary packages for function
    import os
    import numpy as np
    import rasterio
    from rasterio.fill import fillnodata
    
    # Create the output directory if it doesn't exist
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    # Read the DSM and DTM rasters
    dsm = rasterio.open(dsm_file, driver='GTiff')
    dtm = rasterio.open(dtm_file, driver='GTiff')
    
    dsm_data = dsm.read()
    dtm_data = dtm.read()
    
    # Set our nodata to np.nan
    dtm_data[dtm_data == dtm.nodata] = np.nan

    # Create a mask to specify which pixels to fill (0=fill, 1=do not fill)
    dtm_mask = dtm_data.copy()
    dtm_mask[~np.isnan(dtm_data)] = 1
    dtm_mask[np.isnan(dtm_data)] = 0

    # Fill missing values
    dtm_data = fillnodata(dtm_data, mask = dtm_mask) 
    
    # Calculate the canopy height model through subtraction of the DTM from the DSM
    chm_ahn3 = dsm_data - dtm_data

    # Create a copy of the metadata of the DSM raster
    kwargs = dsm.meta   
    
    # Specify the output file path
    chm_file = os.path.join(out_dir, filename)
    
    # Write the CHM to file
    with rasterio.open(chm_file, 'w', **kwargs) as file:
        file.write(chm_ahn3.astype(rasterio.float32))
        

def remove_buildings(in_dir, buildings_file, out_dir, filename):
    """
    Removes buildings from a raster using a shapefile representing buildings.

    INPUTS:
        in_dir (str): Path to the input raster file.
        buildings_file (str): Path to the shapefile containing buildings.
        out_dir (str): Path to save the output raster file with buildings removed.
        filename (str): The name of the output raster file
        
    OUTPUT:
        This function does not return. The raster with removed buildings is saved in the specified directory.
    
    POTENTIAL RISKS & ASSUMPTIONS:
        - The input raster and shapefile have compatible coordinate reference systems (CRS).
        - Assumes -9999 is not a valid raster value i.e. it signifies nodata.
        - Assumes that the raster data is read from band 1 of the raster.
        
    """
    
    # Import necessary packages for function
    from rasterio.mask import mask
    import rasterio
    import geopandas as gpd
    import os
    
    # Create the output directory if it doesn't exist
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    # Open the canopy height model
    with rasterio.open(in_dir) as src:
        # Read the buildings shapefile into a GeoDataFrame
        buildings = gpd.read_file(buildings_file)
    
        # Get the geometries from the GeoDataFrame
        shapes = buildings.geometry.tolist()
    
        # Mask the canopy height model using the building geometries
        masked_data, masked_transform = mask(src, shapes, invert=True, nodata=-9999)
    
        # Update metadata for the masked data
        masked_meta = src.meta.copy()
        masked_meta.update({
            'height': masked_data.shape[1],
            'width': masked_data.shape[2],
            'transform': masked_transform
        })
    
        # Write the masked canopy height model to a new file
        chm_nobuildings_file = os.path.join(out_dir, filename)
        with rasterio.open(chm_nobuildings_file, 'w', **masked_meta) as dst:
            dst.write(masked_data)


#### VEGETATION INDICES ####

def calculate_vi(in_dir, out_dir, norm_factor=10000):
    """
    Calculates the following vegetation indices per tiff file: Normalised Difference Vegetation Index (NDVI),
    Soil Adjusted Vegetation Index (SAVI), Green Chlorophyll Index (GCI), Normalised Difference Red Edge Index (NDRE).

    INPUT:
        in_dir (str): The path to the directory in which the tiff files are located.
        out_dir (str): The path to the directory where the calculated VI rasters will be saved in.
        norm_factor (str): This is an integer or float which is the maximum value of the blue band. It normalized all the bands so they can 
                        be used in calculating the vegetation indices. The default value is 10000.
    OUTPUT:
        This function does not return. It saves the tiff files to the specified output folder.

    POTENTIAL RISKS & ASSUMPTIONS:
        - This function only works on rasters which have 5 bands in the order of Blue, Green, red, red-edge, NIR.
          If this is different for your rasters, the code needs to be modified. 
        - Division by 0: The calculations of the vegetation indices make it possible to divide by 0. 
          This function removes the nodata from the file so potential division by 0 is very small. 
          However, it is still a possibility.
        - The code assumes that the normalization factor is the same for each band and each image.
        - The code also does not provide extensive error handling
        
    """
    # Import necessary packages for function
    import os
    import rasterio
    import numpy as np
    
    # Create the output directory if it doesn't exist
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    # Create a list of vegetation indices and a list of .tif in the specified input directory
    vi_list = ["ndvi", "savi", "gci", "ndre"]
    tif_files = [file for file in os.listdir(in_dir) if file.endswith(".tif")]
    
    # Loop through all the rasters in the data folder
    for file in tif_files:
        # Construct the file path
        file_path = os.path.join(in_dir, file)
            
        # Open the raster dataset
        a_dataset = rasterio.open(file_path)
        
        # Get the nodata values for the raster dataset
        nodata_value = a_dataset.nodata
        
        # Loop through all the vegetation indices which you want to run
        for vi in vi_list:
            # Create a folder for each vegetation index
            vi_folder = os.path.join(out_dir, vi)
            os.makedirs(vi_folder, exist_ok=True)
                                                  
            # Read the individual bands and convert them to floating-point
            # Blue band
            a_blue = a_dataset.read(1).astype('float32')  
            # Green band
            a_green = a_dataset.read(2).astype('float32')  
            # Red band
            a_red = a_dataset.read(3).astype('float32')  
            # Red Edge Band
            a_rededge = a_dataset.read(4).astype('float32')
            # NIR band
            a_nir = a_dataset.read(5).astype('float32')  
            
            # Mask out the no data values for each band
            a_blue[a_blue == nodata_value] = np.nan
            a_green[a_green == nodata_value] = np.nan
            a_red[a_red == nodata_value] = np.nan
            a_rededge[a_rededge == nodata_value] = np.nan
            a_nir[a_nir == nodata_value] = np.nan

            # Normalize the bands to a common range (e.g., 0-1)
            a_blue = a_blue / norm_factor  
            a_green = a_green / norm_factor
            a_red = a_red / norm_factor
            a_rededge = a_rededge / norm_factor
            a_nir = a_nir / norm_factor

            # Calculate vegetation indices
            # NDVI
            if vi.lower() == 'ndvi':
                denominator = a_nir + a_red
                # Handle potential zero division error
                a_index = np.true_divide((a_nir - a_red), denominator)
            
            # GCI
            elif vi.lower() == 'gci':
                denominator = a_nir + a_green
                # Handle potential zero division error
                a_index = np.true_divide((a_nir - a_green), denominator)
            
            # SAVI
            elif vi.lower() == 'savi':
                # Define the soil adjustment factor (L)
                L = 0.5
                a_index = ((a_nir - a_red) / (a_nir + a_red + L)) * (1 + L)
                
            # NDRE
            elif vi.lower() == 'ndre':
                denominator = a_nir + a_rededge
                a_index = np.true_divide((a_nir - a_rededge), denominator)
                
            # Retrieve the CRS information from the original raster dataset
            crs = a_dataset.crs
            # Retrieve the affine transformation information
            transform = a_dataset.transform  
            
            # Specify the output file path for each VI raster
            output_path = ''
            if vi.lower() == 'ndvi':
                output_path = os.path.join(vi_folder, f"ndvi_{file}")
            elif vi.lower() == 'dvi':
                output_path = os.path.join(vi_folder, f"dvi_{file}")
            elif vi.lower() == 'gci':
                output_path = os.path.join(vi_folder, f"gci_{file}")
            elif vi.lower() == 'savi':
                output_path = os.path.join(vi_folder, f"savi_{file}")
            elif vi.lower() == 'ndre':
                output_path = os.path.join(vi_folder, f"ndre_{file}")
           
            # Create the output raster file
            with rasterio.open(
                    output_path,
                    'w',
                    driver='GTiff',
                    height=a_index.shape[0],
                    width=a_index.shape[1],
                    # Number of bands
                    count=1,  
                    dtype=a_index.dtype,
                    # Set the transformation and CRS information
                    crs=crs,
                    transform=transform,  
            ) as dst:
                # Write the VI array to the raster
                dst.write(a_index, 1)  

def extract_trees_from_VI(in_dir, out_dir, CHM_file):
    """
    Filter vegetation indices (VI) by masking with a canopy height model to extract trees only, and save the filtered rasters
    to an output folder.
    
    INPUT:
        in_dir (str): The folder path containing the vegetation index tiff files.
        out_dir (str): The folder path to store the filtered rasters.
        CHM_file (str): The file path to the canopy height model (CHM) raster.
        
    OUTPUT:
        This function does not return. It saves the filtered vegetation index rasters to the specified output folder.
        
    POTENTIAL RISKS & ASSUMPTIONS:
        - The function assumes that the input and output directories are valid and accessible.
        - The vegetation index TIFF files in the input directory should be valid raster files with compatible projection and dimensions.
        
    """
   
    # Import necessary packages for function
    import rasterio
    import numpy as np
    import os
    from rasterio.warp import Resampling
    import glob
    
    # Specify the output folder path for the filtered rasters
    os.makedirs(out_dir, exist_ok=True)
    
    # Get the list of subfolders in the vegetation indices folder
    vi_subfolders = glob.glob(os.path.join(in_dir, "*"))

    # Loop through each subfolder
    for subfolder in vi_subfolders:
        # Get the vegetation index type from the subfolder name
        vi_type = os.path.basename(subfolder)
        
        # Create the corresponding subfolder in the filtered_output_folder
        output_subfolder = os.path.join(out_dir, vi_type)
        os.makedirs(output_subfolder, exist_ok=True)
        
        # Get the list of TIFF files in the subfolder
        tif_files = glob.glob(os.path.join(subfolder, "*.tif"))
        
        # Extract only the file names
        tif_files = [os.path.basename(file) for file in tif_files]
        
        # Loop through each TIFF file
        for tif_file in tif_files:
            # Construct the file paths
            vi_path = os.path.join(subfolder, tif_file)
            output_path = os.path.join(output_subfolder, tif_file)

            # Specify the NODATA value
            nodata_value = np.nan

            # Open the VI raster dataset in read mode
            with rasterio.open(vi_path) as vi_src:
                # Read the VI raster values
                vi_values = vi_src.read(1)

                # Open the canopy height model raster dataset in read mode
                with rasterio.open(CHM_file) as canopy_height_src:
                    # Read the canopy height model raster values
                    canopy_height_values = canopy_height_src.read(1)
                    canopy_height_transform = canopy_height_src.transform
                    canopy_height_crs = canopy_height_src.crs

                    # Resize the canopy height model raster to match the shape of the VI raster
                    canopy_height_resized = np.empty_like(vi_values)
                    rasterio.warp.reproject(
                        source=canopy_height_values,
                        destination=canopy_height_resized,
                        src_transform=canopy_height_transform,
                        src_crs=canopy_height_crs,
                        dst_transform=vi_src.transform,
                        dst_crs=vi_src.crs,
                        resampling=Resampling.bilinear
                    )

                    # Mask the VI values using the resized canopy height model
                    masked_vi_values = np.where(canopy_height_resized > 3, vi_values, nodata_value)

                    # Update the NODATA value in the VI raster metadata
                    vi_metadata = vi_src.meta
                    vi_metadata.update(nodata=nodata_value)

                    # Create a new raster file with the masked VI values
                    with rasterio.open(output_path, 'w', **vi_metadata) as filtered_dst:
                        filtered_dst.write(masked_vi_values, 1)



def calculate_average_vi(in_dir, out_dir, points):
    """
   Calculates the average vegetation index (VI) values within buffered areas around points of interest, 
   using filtered rasters stored in a specified input directory. The results are saved as CSV files 
   for each vegetation index in a specified output directory.
   
   INPUT:
     in_dir (str): The folder path containing the filtered vegetation index TIFF files organized 
                   by vegetation index type (e.g., "ndvi", "savi", "gci", "ndre").
     out_dir (str): The folder path to store the output CSV files containing the average VI values.
     points (str): The file path to a shapefile containing the points of interest.
     
   OUTPUT:
     This function does not return. It saves the average VI values as CSV files in the specified output directory.
     
   POTENTIAL RISKS & ASSUMPTIONS:
     - This function assumes that the tree dataset has a point geometry, it could be adjusted to intake different geometry types
     - The function assumes that the in_dir, out_dir, and points are valid and accessible.
      - The function assumes that the date part within the filename of the filtered rasters always follows 
         the format "YYYY-MM-DD" and there are no other parts in the filename that match this pattern.
     - The vegetation index TIFF files in in_dir should be valid raster files with compatible projection and dimensions.
     - The points shapefile should be a valid file in a supported format and contain the necessary attribute data.
     
    """
    
    # Import necessary packages for function
    import os
    import re
    import numpy as np
    import geopandas as gpd
    import rasterio
    
    # Create the Output_bufferdata folder if it doesn't exist
    os.makedirs(out_dir, exist_ok=True)
    
    # Load the shapefile as a GeoDataFrame
    points_gdf = gpd.read_file(points)
        
    vi_lst = ["ndvi", "savi", "gci", "ndre"]
    
    # Loop through each vegetation index
    for vi in vi_lst:
        # Create a DataFrame to store the results
        result_dataframe = points_gdf.copy()
        
        # Get the folder path for the current vegetation index
        vi_folder = os.path.join(in_dir, vi)
        
        # Get the list of filtered TIFF files in the folder for the current vegetation index
        tif_files_filtered = [file for file in os.listdir(vi_folder) if file.lower().startswith(vi.lower()) and file.endswith(".tif")]
            
        # Loop through each filtered TIFF file for the current vegetation index
        for tif_file_filtered in tif_files_filtered:
            # Construct the file path of the filtered raster
            filtered_raster_path = os.path.join(vi_folder, tif_file_filtered)
    
            # Load the filtered raster
            with rasterio.open(filtered_raster_path) as src:
                # Define the buffer distance in meters
                buffer_distance = 3
    
                # Create a buffer around each point
                points_buffered = result_dataframe.copy()
                points_buffered.geometry = points_buffered.geometry.buffer(buffer_distance)
    
                # Calculate the average VI value within each buffer
                average_vi_values = []
                for _, point in result_dataframe.iterrows():
                    # Get the pixel coordinates of the buffered area
                    minx, miny, maxx, maxy = points_buffered.loc[points_buffered.index == point.name].geometry.bounds.values[0]
                    row_start, col_start = rasterio.transform.rowcol(src.transform, minx, maxy)
                    row_end, col_end = rasterio.transform.rowcol(src.transform, maxx, miny)
    
                    # Read the raster values within the buffered area
                    window = ((row_start, row_end + 1), (col_start, col_end + 1))
                    raster_values = src.read(1, window=window)
    
                    # Calculate the average VI value, omitting NaN values
                    valid_values = raster_values[~np.isnan(raster_values)]
                    average_vi = np.mean(valid_values)
    
                    # Store the average VI value in the list
                    average_vi_values.append(average_vi)
    
                # Extract the date part from the filename
                date_part = re.search(r"\d{4}-\d{2}-\d{2}", tif_file_filtered).group()
    
                # Add the average VI values to the result DataFrame using the date as the column name
                result_dataframe[date_part] = average_vi_values
    
    
        # Save the result DataFrame as a CSV file for the current vegetation index
        output_filename = f"average_vi_results_{vi}.csv"
        output_path = os.path.join(out_dir, output_filename)
        result_dataframe.to_csv(output_path, index=False)
        
        
#### TREND ANALYSIS ####

def prepare_data(in_dir, out_dir):
    """
    Prepares the data for linear regression by normalising the dates and filtering out rows with excessive null values 
    in CSV files within a specified input directory. The processed data is then saved as new CSV files in the
    specified output directory.
    
    INPUT: 
        in_dir (str): The folder path containing the input CSV files.
        out_dir (str): The folder path to store the output CSV files.
    
    OUTPUT:
        This function does not return. It saves the processed CSV files in the specified output directory.
    
    POTENTIAL RISKS & ASSUMPTIONS:
    - The function assumes that the input and output directories are valid and accessible.
    - The function assumes that the CSV files have headers containing date information in the format "YYYY-MM-DD".
    - The function assumes that the CSV files have columns with specific names to be dropped ('postgis_ty', 'BOOMEIGENA', 'CONTROLE_P', 'SOORT_NED', 'RAYON', 'CONFLICTEN', 'ORIG_FID').
    - The function assumes that the values in the CSV files after the 4th column represent the data to be analyzed.
    - The function removes rows with more than 5 null values from the processed data which may remove some valid trees.
    
    """

    # Import necessary packages for function
    import os
    import re
    import pandas as pd
    
    # Create the Output_bufferdata folder if it doesn't exist
    os.makedirs(out_dir, exist_ok=True)
    
    # Loop through the CSVs in the input directory
    csv_files = [file for file in os.listdir(in_dir) if file.endswith(".csv")]
    
    # Loop through all the rasters in the data folder
    for file in csv_files:
        # Construct the file path
        file_path = os.path.join(in_dir, file)
                
        # Open the csv dataframe
        df = pd.read_csv(file_path)
            
        df.drop(['postgis_ty', 'BOOMEIGENA', 'CONTROLE_P', 
                 'SOORT_NED', 'RAYON', 'CONFLICTEN', 'ORIG_FID',
                 ], axis=1, inplace=True) # Example of columns to be dropped in Wageningen tree-point dataset

        ##### Normalise the dates from start - end monitoring period to 0 - 1 ####

        # Extract and convert dates from the headers
        date_strings = []
        for header in df.columns:
            # Find the date pattern in the header using regular expression
            match = re.findall(r'\d{4}-\d{2}-\d{2}', header)
            # If a match is found, append the date string to the list
            if match:
                date_strings.append(match[0])
                
        # Convert the date strings to pandas datetime format
        dates = pd.to_datetime(date_strings, format='%Y-%m-%d')

        # Find the first and last dates
        first_date = min(dates)
        last_date = max(dates)

        # Calculate the range of dates
        date_range = (last_date - first_date)

        # Normalize the dates between 0 and 1
        normalized_dates = [(date - first_date) / date_range for date in dates]

        # Create a dictionary to map the original date columns to the normalized dates
        date_column_mapping = {column: normalized_date for column, normalized_date in zip(df.columns[5:], normalized_dates) if re.findall(r'\d{4}-\d{2}-\d{2}', column)} # Adjust location to your own dataset

        # Update the DataFrame with the normalized dates and drop NA values
        df = df.rename(columns=date_column_mapping)

        # Loop through each row in the DataFrame
        for index, row in df.iterrows():
            # Extract the values from the row
            y = row.iloc[4:].values
            
            num_null_values = pd.isna(y).sum()
            if num_null_values > 5:
                df = df.drop(index)
                        
        # Reset the index of the DataFrame
        df = df.reset_index(drop=True)
        
        # Save the result DataFrame as a CSV file for the current vegetation index
        output_filename = f"{file}"
        output_path = os.path.join(out_dir, output_filename)
        df.to_csv(output_path, index=False)
        

def linear_regression_slopes (in_dir, out_dir, bounds):
    """
    Calculates the linear regression slopes for vegetation indices within the specified bounding box,
    using CSV files stored in the input directory. The results are saved as shapefiles containing the
    regression slopes and classification based on the slopes.
    
    INPUT:
        in_dir (str): The folder path containing the input CSV files of vegetation indices.
        out_dir (str): The folder path to store the output shapefiles.
        bounds (str): The bounding box coordinates in the format "xmin, ymin, xmax, ymax" defining
                      the spatial extent of the analysis area.
                      
     OUTPUT:
        This function does not return any value. It saves the regression slopes and classification as
        shapefiles in the specified output directory.
        
    
    POTENTIAL RISKS & ASSUMPTIONS:
        - The function assumes that the in_dir and out_dir are valid and accessible.
        - The input CSV files should be properly formatted as follows:
            + The CSV files should contain vegetation index data with each column representing a tree and
                the rows representing dates or time intervals.
            + The CSV files should have a column named 'geometry' containing the coordinates of each tree.
        - The CSV files should have the first five rows as non-data rows, such as headers or metadata.
        
    """
    
    
    # Import necessary packages for function
    import os
    import re
    import numpy as np
    import pandas as pd
    from sklearn.linear_model import LinearRegression
    import geopandas as gpd
    from shapely.geometry import Point
    
    # Create the Output_bufferdata folder if it doesn't exist
    os.makedirs(out_dir, exist_ok=True)
    
    # Loop through the CSVs in the input directory
    csv_files = [file for file in os.listdir(in_dir) if file.endswith(".csv")]
    
    # Loop through all the rasters in the data folder
    for file in csv_files:
        # Extract the vegetation index from the file name
        index_match = re.search(r'average_vi_results_(.+)\.csv', file)
        if index_match:
            vegetation_index = index_match.group(1)
        else:
            continue  # Skip files that don't match the expected pattern
        # Construct the file path
        file_path = os.path.join(in_dir, file)
        
        df = pd.read_csv(file_path)
        
        # Transpose the dataframe
        df = df.transpose()

        # Create a list to store the new dataframes
        tree_dfs = []

        # Iterate over each tree column
        for column in df.columns:
            # Create a new dataframe for the current tree
            tree_df = pd.DataFrame({'Date': df.index[5:], 'VI': df[column][5:]}) # Adjust location to your own dataset
            
            # Reset index
            tree_df.reset_index(drop=True, inplace=True)
            
            # Append the tree dataframe to the list
            tree_dfs.append(tree_df)
            
            
        # Iterate over each tree dataframe to remove NaN observations
        for i, tree_df in enumerate(tree_dfs):
            # Remove observations with NaN values in any column
            tree_df = tree_df.dropna(how='any')
            
            # Update the modified dataframe in the tree_dfs list
            tree_dfs[i] = tree_df


        ####  Iterate over each tree dataframe to calculate weights and add them to that dataframe ####

        for i, tree_df in enumerate(tree_dfs):
            tree_df['Date'] = pd.to_numeric(tree_df['Date'])
            previous_diffs = tree_df['Date'].diff()
            subsequent_diffs = abs(tree_df['Date'].diff(periods=-1))

            # Calculate the mean of the previous and subsequent time differences
            mean_dist = (previous_diffs + subsequent_diffs) / 2
            
            ## Mean not possible for first and last observations
            
            # For the first point, use the subsequent time difference as weight
            mean_dist.iloc[0] = subsequent_diffs.iloc[0]
            
            # For the last point, use the previous time difference as weight
            mean_dist.iloc[-1] = previous_diffs.iloc[-1]

            # Normalize the weights to ensure their sum is equal to 1
            weights = mean_dist/ mean_dist.sum()

            # Assign the weights to the DataFrame
            tree_df['weights'] = weights
            
            # Transpose the dataframe
            tree_df = tree_df.transpose()
            
            # Set the 'Date' row as the headers
            tree_df.columns = tree_df.iloc[0]

           # Remove the 'Date' row
            tree_df = tree_df[1:]

           # Update the tree dataframe in the tree_dfs list
            tree_dfs[i] = tree_df
         
        # Create an instance of the LinearRegression model
        regression_model = LinearRegression() 
        slopes = []
            
        # Iterate over each tree dataframe
        for tree_df in tree_dfs:
            # Extract the independent variable and reshape it
            X = tree_df.columns.values.reshape(-1,1)
            # Extract the dependent variable and reshape it 
            y = tree_df.iloc[0].values.reshape(-1,1)
            # Extract the weights
            w = tree_df.iloc[1].values
            
            # Fit the linear regression model with weighted samples
            regression_model.fit(X,y,sample_weight=w)
            
            # Get the slope coefficient
            slope=regression_model.coef_[0][0]
            
            # Append the slope to the list of slopes
            slopes.append(slope)
        
        # Transpose the original dataframe for further processing
        df = df.transpose()
        
        # Add the slope as a new column in the original dataframe
        df['slope'] = slopes
        
        ### MAP ###
        # Create a GeoDataFrame from the DataFrame
        
        # Remove non-numeric characters from geometry column
        df['geometry'] = df['geometry'].apply(lambda row: re.sub('[^0-9. ]', '', row))

        # Split coordinates and convert to float
        df['geometry'] = df['geometry'].apply(lambda row: [float(coord) for coord in row.split()])

        # Create Point objects from coordinates
        df['geometry'] = df['geometry'].apply(lambda coords: Point(coords[0], coords[1]))
        
        # Select specific columns from the dataframe
        df = df.iloc[:, [*range(5), -1]]
        
        # Create a GeoDataFrame with the selected columns and the geometry column
        gdf = gpd.GeoDataFrame(df, geometry='geometry')
        
        # Calculate the standard deviation of the slope variable
        slope_std = gdf['slope'].std()
        
        # Classify the values based on the criteria
        gdf['Status'] = np.where(
            (gdf['slope'] >= slope_std) & (gdf['slope'] <= -slope_std), 'Stable',
            np.where(gdf['slope'] < -slope_std, 'Declining',
            np.where(gdf['slope'] > slope_std, 'Increasing', 'Stable'))
            )
        
        # Save the result DataFrame as a CSV file for the current vegetation index
        output_filename = f"regression_{vegetation_index}.shp"
        output_path = os.path.join(out_dir, output_filename)
        gdf.to_file(output_path, driver='ESRI Shapefile', crs='EPSG:28992')    
