# UrbanTreeVitality
Investigating individual tree health trends in urban areas in the Netherlands

## Introduction
The script focuses on assessing tree vitality trends in urban environments. To achieve this, custom functions in Python are developed, which are called by a main script. The methodology is designed to be scalable for tree vitality monitoring in any urban area in the Netherlands.

High resolution satellite data, such as PlanetScope imagery, is obtained and processed to correct for atmospheric interference. Vegetation indices, such as NDVI, SAVI, GCI, and NDRE, are calculated from the satellite imagery to measure tree health and stress. Tree segmentation techniques are applied to calculate average vegetation index values per tree. 

The trend analysis involves extracting vegetation index values for each tree location at different acquisition dates. This is done by creating a buffer around each tree point, but could be further adjusted to intake different geometry types. Data preprocessing techniques, including handling missing values, are applied to ensure the accuracy of the time-series analysis. Weighted linear regression accounts for the uneven spread of observation points in time by assigning weights based on the temporal density of the measurements. By analyzing the slope, it becomes possible to determine whether the trees are experiencing improvements or declines in their health. 

This script returns shapefiles of tree point data with the slope values obtained through the regression analysis for each vegetation index, and classified based on the trend in vitality. 

<img src="https://i.imgur.com/ZWUa0vz.png" width="600"/><br>
*Fig. 1: Classified tree-point data set in Wageningen*

<img src="https://i.imgur.com/k2pYPKq.png" width="500"/><br>
*Fig. 2: Example of English Oak status for GCI*

## File structure
- `main.py`: This file contains the main script for the project. It will handle creating of the `data` , `intermediate` and `output` folders.

- `functions.py`: This file contains functions or utility code that may be used by the `main.py` script.

- `data/`: This directory is used to store data files required for the running of the script.  It is organised into further subdirectories as follows:

	-- `raster/` \
	-- `study_extent/` \
	-- `tree_points/` \
	-- `buildings/` 

- `intermediate/`: This directory is used to store intermediate files. The contents of this folder are not final deliverables, and can therefore be deleted.

- `output/`: This directory is used to store the project deliverables. 

## How to install and run the project
1. Clone this repository to your local repository. You can use Git GUI (or command line) for this.
2. In the terminal, use create a new working environment with the file urbantree.yml. Activate the new environment.
3. Place your satellite data imagery in the `data/raster` folder, a study extent shapefile in the `data/study_extent` folder, the tree point data in `data/tree_points`, and the buildings shapefile in `data/buildings`. 
4. Make the necessary adjustments in functions.py according to your tree point dataset. In the prepare data function the column removal example is based on the Wageningen tree point dataset, but should be adjusted per tree geometry dataset used.

```python
    # Open the csv dataframe
    df = pd.read_csv(file_path)
        
    df.drop(['postgis_ty', 'BOOMEIGENA', 'CONTROLE_P', 'SOORT_NED', 'RAYON', 'CONFLICTEN', 'ORIG_FID'], axis=1, inplace=True)
```

5. Run the main.py file.

- *If the satellite data to be used is previous to 2019, using AHN3 to extract the Canopy Height Model is recommended. AHN4 data is only acquired from 2020 onwards.*
- *The Web Feature Service for buildings from pdok.nl presents striping, therefore this [layer](https://www.pdok.nl/downloads/-/article/basisregistratie-adressen-en-gebouwen-ba-1) has been manually downloaded, clipped to the study extent, and filtered for buildings with GIS software*

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Authors 
- *Annabel Burg*
- *Marthe Deij*
- *Erin Driscoll*
- *Jesper Linderhof*
- *Jorge Portugués Fernández del Castillo*
- *Marilena Willemsen*
